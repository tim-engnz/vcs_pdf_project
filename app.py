import pandas as pd
import requests
import json
import urllib.request

from bs4 import BeautifulSoup
import re
# import urllib2
import aspose.words as aw
from lxml import etree
import requests
from pypdf import PdfReader

df_projects = pd.read_csv("./projectids (1).csv")

attributes =[]

for c in ["2902","2909","2903","2629"]:
    rs = getPoly(value=c)
    for item in rs['polygons']:
        attributes+=item['attributes']
    




def getPoly(name="VerraProjectId", value=""):
    
    url = "https://api.geopixel.earth/GeoPolygon/Search"

    payload = json.dumps({
      "attribute": {
        "name": str(name),
        "value": str(value)
      }
    })
    headers = {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

#     print(response.text)
    return response.json()

pd_poly = pd.DataFrame(attributes)
selected_ProjectDescriptionDoc_df = pd_poly.loc[pd_poly.name == "ProjectDescriptionDoc"]

def getRequirements(soup):
    table = soup.find_all('table')[0]
    t=[]
    for tr in table.findAll('tr'):
        p=[]
        for a in tr.findAll('td'):
            p.append(a.text)
        t.append(p)
    return t

def getDescription(_text):
    start='1.11 Description of the Project Activity'
    end='1.12 Project Location'
    l =_text[_text.index(start)+len(start):-_text.index(end)]
    
    return l

def load_save_file(row):
    _url = row['value']
    _id = row['id']
    print(_url)

    response = requests.get(_url)
    content_type = r.headers.get('content-type')
    my_raw_data = response.content
    
    _filename = f"data/{_id}.pdf"
    _html_filename = f"data/{_id}.html"
    with open(_filename, 'wb') as my_data:
        my_data.write(my_raw_data)

    
    
def process(row):
#     _url = row['value']
    _id = row['id']
    print(_id)
#     print(_url)

    _filename = f"data/{_id}.pdf"
    _html_filename = f"data/{_id}.html"

    _reader = PdfReader(_filename)
    _text = ""
    for _page in _reader.pages:
        _text += _page.extract_text()


    _soup = BeautifulSoup(open(_html_filename).read())
#     dom = etree.HTML(str(soup))
    
    a1=getDescription(_text)
    a2=getRequirements(_soup)
    return a1,a2


selected_ProjectDescriptionDoc_df[['result_descriptions','result_requirements']] = selected_ProjectDescriptionDoc_df.apply(process,axis=1, result_type="expand")

