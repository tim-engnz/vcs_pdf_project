FROM python:3.8.3

WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . .
COPY run.sh /app
CMD [ "sh", "run.sh"]


